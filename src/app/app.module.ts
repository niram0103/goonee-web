import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonLayoutComponent } from './layouts/common-layout/common-layout.component';
import {HeaderModule} from './layouts/header/header.module';
import {FooterModule} from './layouts/footer/footer.module';
import { SideBarComponent } from './layouts/side-bar/side-bar.component';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
@NgModule({
  declarations: [
    AppComponent,
    CommonLayoutComponent,
    SideBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HeaderModule,
    FooterModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    CommonLayoutComponent
  ]
})
export class AppModule { }
