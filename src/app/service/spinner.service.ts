import {Injectable, NgZone} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner'

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  stack:number = 0;
  constructor(public spinner:NgxSpinnerService,public ngZone:NgZone) { }

  show(){
    this.ngZone.run(()=>{
      this.spinner.show().then(() => {this.stack++;});
    });
  }

  hide(){
    this.ngZone.run(()=> {
      this.spinner.hide().then(() => {this.stack--;});
    });
  }
}
