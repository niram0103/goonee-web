import {Injectable} from '@angular/core';
import {configuration} from '../configration';
import { Subject } from 'rxjs';
import {EncryptionService} from "./encryption.service";
const AUTH = configuration.AUTH_KEY;


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService{
  public data: AuthData = new AuthData();
  encrypt : any;
  private userLoggedIn = new Subject<boolean>();

  constructor(public encryptionService:EncryptionService) {
    if (localStorage.getItem(AUTH) != null){
      let decrypt = this.encryptionService.decrypt(localStorage.getItem(AUTH));
      this.data =JSON.parse(decrypt);
    }else{
      localStorage.clear();
    }
  }


  getToken(){
    if (localStorage.getItem(AUTH) != null){
      let decrypt = this.encryptionService.decrypt(localStorage.getItem(AUTH));
      this.data =JSON.parse(decrypt);
    }
    return this.data.token;
  }

  getVerifyToken(){
    let verifyToken;
    if (localStorage.getItem('verifyToken') != null){
      verifyToken =localStorage.getItem('verifyToken')
    }
    return verifyToken;
  }

  getAuthDetail(){
    if (localStorage.getItem(AUTH) != null){
      let decrypt = this.encryptionService.decrypt(localStorage.getItem(AUTH));
      this.data =JSON.parse(decrypt);
    }
    return this.data;
  }

  setAuth(data:any){
    this.data = data;
    localStorage.removeItem(AUTH);
    this.encrypt = this.encryptionService.encrypt(JSON.stringify(data))
    localStorage.setItem(AUTH, this.encrypt);
    let decrypt = this.encryptionService.decrypt(localStorage.getItem(AUTH));
    this.data =JSON.parse(decrypt);
    this.getAuthDetail();
  }

  logout()
  {
    localStorage.removeItem(AUTH);
    localStorage.removeItem('verifyToken');
    location.reload();
  }
}



export class AuthData{
  isLoggedIn:boolean = false
  token: string='';
  verifyToken: string='';
  authDetail:any
}
