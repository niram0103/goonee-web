import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonLayoutComponent } from './common-layout.component';
import {FooterModule} from "../footer/footer.module";
import {RouterModule} from "@angular/router";
import {HeaderModule} from "../header/header.module";


@NgModule({
  declarations: [
    CommonLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    FooterModule,
  ],
  exports: [
    CommonLayoutComponent,
  ],
})
export class CommonLayoutModule { }
