export class Configuration {
  BASE_URL = '';
  AUTH_KEY: string = "authDetail";

  constructor(public state: projectState) {
    if (this.state == "live") {
      this.BASE_URL = 'http://127.0.0.1:8000/api/';
    }else if (this.state == "local") {
      this.BASE_URL = 'http://127.0.0.1:8000/api/';
    }
  }
}

type projectState =  'live' | "local" ;
export const configuration = new Configuration('live');
