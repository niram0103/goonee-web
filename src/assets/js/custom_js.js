(function ($) {
    'use strict';
    $(function () {
        
        $('.dropdown-menu .nav-tabs .nav-link').on("click.bs.dropdown", function (e) {
            $(this).tab('show');
            e.stopPropagation();
        });

        if ($("#performaneLine").length) {
            var graphGradient = document.getElementById("performaneLine").getContext('2d');
            var graphGradient2 = document.getElementById("performaneLine").getContext('2d');
            var saleGradientBg = graphGradient.createLinearGradient(5, 0, 5, 100);
            saleGradientBg.addColorStop(0, 'rgba(108, 96, 225, 0.18)');
            saleGradientBg.addColorStop(1, 'rgba(108, 96, 225, 0.02)');
            var saleGradientBg2 = graphGradient2.createLinearGradient(100, 0, 50, 150);
            saleGradientBg2.addColorStop(0, 'rgba(108, 96, 225, 0.19)');
            saleGradientBg2.addColorStop(1, 'rgba(108, 96, 225, 0.19)');
            var salesTopData = {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                        label: 'Earnings',
                        data: [50, 110, 60, 290, 200, 115, 130, 170, 90, 210, 240, 280,400,600,200,400],
                        backgroundColor: saleGradientBg,
                        borderColor: [
                            '#63BE6B',
                        ],
                        borderWidth: 1.5,
                        fill: true, // 3: no fill
                        pointBorderWidth: 1,
                        pointRadius: [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
                        pointHoverRadius: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                        pointBackgroundColor: ['#6C60E1)', '#6C60E1', '#6C60E1', '#6C60E1', '#6C60E1)', '#6C60E1', '#6C60E1', '#6C60E1', '#6C60E1)', '#6C60E1', '#6C60E1', '#6C60E1', '#6C60E1)'],
                        pointBorderColor: ['#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', ],
                    }, {
                        label: 'Total Sales',
                        data: [30, 150, 190, 250, 120, 150, 130, 20, 30, 15, 40, 95,30,56,87],
                        backgroundColor: saleGradientBg2,
                        borderColor: [
                            '#6C60E1',
                        ],
                        borderWidth: 1.5,
                        fill: true, // 3: no fill
                        pointBorderWidth: 1,
                        pointRadius: [0, 0, 0, 4, 0],
                        pointHoverRadius: [0, 0, 0, 2, 0],
                        pointBackgroundColor: ['#63BE6B)', '#63BE6B', '#63BE6B', '#63BE6B', '#63BE6B)', '#63BE6B', '#63BE6B', '#63BE6B', '#63BE6B)', '#63BE6B', '#63BE6B', '#63BE6B', '#63BE6B)'],
                        pointBorderColor: ['#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', ],
                    }]
            };

            var salesTopOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            gridLines: {
                                display: true,
                                drawBorder: false,
                                color: "#F0F0F0",
                                zeroLineColor: '#F0F0F0',
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 4,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                    xAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 7,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                },
                legend: false,
                legendCallback: function (chart) {
                    var text = [];
                    text.push('<div class="chartjs-legend"><ul class="custom_chart-label custom_chart-inline d-flex align-items-start">');
                    for (var i = 0; i < chart.data.datasets.length; i++) {
                        console.log(chart.data.datasets[i]); // see what's inside the obj.
                        text.push('<li>');
                        text.push('<span style="background-color:' + chart.data.datasets[i].borderColor + '">' + '</span>');
                        text.push(chart.data.datasets[i].label);
                        text.push('</li>');
                    }
                    text.push('</ul></div>');
                    return text.join("");
                },

                elements: {
                    line: {
                        tension: 0.4,
                    }
                },
                tooltips: {
                    backgroundColor: '#000',
                }
            }
            var salesTop = new Chart(graphGradient, {
                type: 'line',
                data: salesTopData,
                options: salesTopOptions
            });
            document.getElementById('performance-line-legend').innerHTML = salesTop.generateLegend();
        }
if ($("#doughnutChart").length) {
      var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
      var doughnutPieData = {
        datasets: [{
          data: [25, 50, 25],
          backgroundColor: [
             "#63BE6B",
            "#E3E1F5",
            "#6C60E1"
          ],
          borderColor: [
             "#63BE6B",
            "#E3E1F5",
            "#6C60E1"
          ],
        }],
  
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'One to one',
          'Webinars',
          'Video Courses',
        ]
      };
      var doughnutPieOptions = {
        cutoutPercentage: 50,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: true,
        showScale: true,
        legend: false,
        legendCallback: function (chart) {
          var text = [];
          text.push('<div class="chartjs-legend"><ul class="custom_chart-label">');
          for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
            text.push('<li><span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">');
            text.push('</span>');
            if (chart.data.labels[i]) {
              text.push(chart.data.labels[i]);
            }
            text.push('</li>');
          }
          text.push('</div></ul>');
          return text.join("");
        },
        
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        tooltips: {
          callbacks: {
            title: function(tooltipItem, data) {
              return data['labels'][tooltipItem[0]['index']];
            },
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']];
            }
          },
            
          backgroundColor: '#fff',
          titleFontSize: 12,
          titleFontColor: '#0B0F32',
          bodyFontColor: '#737F8B',
          bodyFontSize: 11,
          displayColors: false
        }
      };
      var doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
      document.getElementById('doughnut-chart-legend').innerHTML = doughnutChart.generateLegend();
    }
        if ($("#status-summary").length) {
            var statusSummaryChartCanvas = document.getElementById("status-summary").getContext('2d');
            ;
            var statusData = {
                labels: ["SUN", "MON", "TUE", "WED", "THU", "FRI"],
                datasets: [{
                        label: '# of Votes',
                        data: [50, 68, 70, 10, 12, 80],
                        backgroundColor: "#ffcc00",
                        borderColor: [
                            '#63BE6B',
                        ],
                        borderWidth: 2,
                        fill: false, // 3: no fill
                        pointBorderWidth: 0,
                        pointRadius: [0, 0, 0, 0, 0, 0],
                        pointHoverRadius: [0, 0, 0, 0, 0, 0],
                    }]
            };

            var statusOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                color: "#F0F0F0"
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 4,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                    xAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 7,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                },
                legend: false,

                elements: {
                    line: {
                        tension: 0.4,
                    }
                },
                tooltips: {
                    backgroundColor: 'rgba(31, 59, 179, 1)',
                }
            }
            var statusSummaryChart = new Chart(statusSummaryChartCanvas, {
                type: 'line',
                data: statusData,
                options: statusOptions
            });
        }

        if ($("#status-summary1").length) {
            var statusSummaryChartCanvas = document.getElementById("status-summary1").getContext('2d');
            ;
            var statusData = {
                labels: ["SUN", "MON", "TUE", "WED", "THU", "FRI"],
                datasets: [{
                        label: '# of Votes',
                        data: [50, 68, 70, 10, 12, 80],
                        backgroundColor: "#ffcc00",
                        borderColor: [
                            '#FF7F56',
                        ],
                        borderWidth: 2,
                        fill: false, // 3: no fill
                        pointBorderWidth: 0,
                        pointRadius: [0, 0, 0, 0, 0, 0],
                        pointHoverRadius: [0, 0, 0, 0, 0, 0],
                    }]
            };

            var statusOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                color: "#F0F0F0"
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 4,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                    xAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 7,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                },
                legend: false,

                elements: {
                    line: {
                        tension: 0.4,
                    }
                },
                tooltips: {
                    backgroundColor: 'rgba(31, 59, 179, 1)',
                }
            }
            var statusSummaryChart = new Chart(statusSummaryChartCanvas, {
                type: 'line',
                data: statusData,
                options: statusOptions
            });
        }

        if ($("#status-summary2").length) {
            var statusSummaryChartCanvas = document.getElementById("status-summary2").getContext('2d');
            ;
            var statusData = {
                labels: ["SUN", "MON", "TUE", "WED", "THU", "FRI"],
                datasets: [{
                        label: '# of Votes',
                        data: [50, 68, 70, 10, 12, 80],
                        backgroundColor: "#ffcc00",
                        borderColor: [
                            '#6C60E1',
                        ],
                        borderWidth: 2,
                        fill: false, // 3: no fill
                        pointBorderWidth: 0,
                        pointRadius: [0, 0, 0, 0, 0, 0],
                        pointHoverRadius: [0, 0, 0, 0, 0, 0],
                    }]
            };

            var statusOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                color: "#F0F0F0"
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 4,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                    xAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                beginAtZero: false,
                                autoSkip: true,
                                maxTicksLimit: 7,
                                fontSize: 10,
                                color: "#6B778C"
                            }
                        }],
                },
                legend: false,

                elements: {
                    line: {
                        tension: 0.4,
                    }
                },
                tooltips: {
                    backgroundColor: 'rgba(31, 59, 179, 1)',
                }
            }
            var statusSummaryChart = new Chart(statusSummaryChartCanvas, {
                type: 'line',
                data: statusData,
                options: statusOptions
            });
        }
    });
})(jQuery);


